import React from "react";
import { render } from "react-dom";
import { createStore } from "redux";
import rootReducer from "./reducers";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";

const store = createStore(rootReducer);
render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
